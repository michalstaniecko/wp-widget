<?php

	class New_Widget extends WP_Widget {

		/**
		 * sets up the widget name  etc
		**/
		public function __construct() {
			$widget_ops = array(
				'classname' => 'new_widget',
				'desription' => 'My new widget',
			);
			parent::__construct('new_widget', 'New Widget', $widget_ops);
		}

		/**
		 * Outputs the content of the widget
		 *
		 * @param array $args
		 * @param array $instance
		 */

		public function widget( $args, $instance) {
			// outputs the content of the widget
			echo $args['before_widget'];
				if (!empty($instance['title'])) {
					echo $args['before_title'].apply_filters( 'widget_title', $instance['title']).$args['after_title'];
				}
				echo $instance['desc'];
				if (!empty($instance['img'])) {
					echo '<div><img src="'.$instance['img'].'"/></div>';
				}
				echo $args['after_widget'];
		}

		/**
		 * outputs the options form on admin
		 *
		 * @param array $instance The widget options
		 */

		public function form($instance) {
			// outputs the options form on admin
			if (isset($instance['title'])) {
				$title=$instance['title'];
			} else {
				$title = __('new title', 'text_domain');
			}
			if (isset($instance['desc'])) {
				$desc = $instance['desc'];
			} else {
				$desc  = __('New description', 'text_domain');
			}
			if (isset($instance['img'])) {
				$img = $instance['img'];
			} else {
				$img = '';
			}
			?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title');?></label>
				<input class="widefat" 
						name="<?php echo $this->get_field_name('title'); ?>"
						id="<?php echo $this->get_field_id('title'); ?>"  
						type="text" 
						value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('desc'); ?>"><?php _e('Opis');?></label>
				<input class="widefat" 
						name="<?php echo $this->get_field_name('desc'); ?>"
						id="<?php echo $this->get_field_id('desc'); ?>"  
						type="text" 
						value="<?php echo esc_attr( $desc ); ?>" />
			</p>
			<p>
				<div class="custom-img">
					<img src="<?php echo $img; ?>" />
				</div>
				<input type="button" 
						class="upload-custom-img " 
						value="Upload image" />
				<input type="button" 
						class="delete-custom-img " 
						value="Remove image" />
				<input type="hidden" 
						name="<?php echo $this->get_field_name('img'); ?>"
						id="<?php echo $this->get_field_id('img');?>"
						class="custom-img-url" 
						value="<?php echo $img; ?>">
			</p>
			<?php
		}

		/**
		* processing widget options on save
		* 
		* @param array $new_instance the new options
		* @param array $old_instance the previous options
		*/

		public function update($new_instance, $old_instance){
			// processing widget options to be saved
			$instance = array();
			$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']): '';
			$instance['desc'] = (!empty($new_instance['desc'])) ? strip_tags($new_instance['desc']): '';
			$instance['img'] = (!empty($new_instance['img'])) ? strip_tags($new_instance['img']) : '';
			return $instance;
		}

	}

	add_action('widgets_init', function() {
		register_widget( 'New_Widget' );
	})
?>