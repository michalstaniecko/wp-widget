jQuery(function($){

  // Set all variables to be used in scope
  var frame,
      metaBox = $('#meta-box-id.postbox'), // Your meta box id here
      addImgLink = metaBox.find('.upload-custom-img'),
      delImgLink = metaBox.find( '.delete-custom-img'),
      imgIdInput = metaBox.find( '.custom-img-id' );
  
  // ADD IMAGE LINK
  $('body').on( 'click', '.upload-custom-img', function( event ){
    
    event.preventDefault();
    var $form = $(this).closest('.widget');
    
    var $this_input = $form.find('.custom-img-url');
    var $this_image = $form.find('.custom-img img');
    // If the media frame already exists, reopen it.
    if ( frame ) {
      frame.open();
      return;
    }
    
    // Create a new media frame
    frame = wp.media({
      title: 'Wybierz lub dodaj zdjęcie',
      button: {
        text: 'Use this media'
      },
      multiple: false // Set to true to allow multiple files to be selected
      
    });

    
    // When an image is selected in the media frame...
    frame.on( 'select', function() {
      
      // Get media attachment details from the frame state
      var attachment = frame.state().get('selection').first().toJSON();

      // Send the attachment URL to our custom image input field.
      $this_image.attr('src', attachment.url);

      // Send the attachment id to our hidden input
      imgIdInput.val( attachment.id );

      // Hide the add image link
      //addImgLink.addClass( 'hidden' );

      // Unhide the remove image link
      delImgLink.removeClass( 'hidden' );
      $this_input.val(attachment.url);
        frame= '';  // reset frame !important
    });

    // Finally, open the modal on click
    frame.open();
  });
  
  
  // DELETE IMAGE LINK
  $('body').on( 'click', '.delete-custom-img',function( event ){

    event.preventDefault();
    var $self = $(this);
    var $form = $(this).closest('form');
    var $this_image = $form.find('img');
    var $this_input = $form.find('.custom-img-url')
    // Clear out the preview image
    $this_image.attr('src', '' );

    // Un-hide the add image link
    //addImgLink.removeClass( 'hidden' );

    // Hide the delete image link

    // Delete the image id from the hidden input
    $this_input.val( '' );

  });

});